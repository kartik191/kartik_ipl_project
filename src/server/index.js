const fs = require('fs');
const path = require('path');

const ipl = require('./ipl');


function removeEmptyLinesAtEnd(data) {
    for (let i = data.length - 1; i >= 0; i--) {
        if (data[i].length > 0 && data[i][0].length > 0) {
            return data.slice(0, i + 1);
        }
    }
    return [];
}

function createObject(data) {
    let header = data[0];
    let newdata = data
        .slice(1)
        .map(row => row.reduce((result, column, j) => {
            result[header[j]] = column;
            return result;
        }, {})
        );
    return newdata;
}

function getData(path) {
    let data = fs.readFileSync(path).toString();

    data = data
        .split('\n')
        .map(element => element.replace('\r','').split(',')); 
    data = removeEmptyLinesAtEnd(data);
    data = createObject(data);

    return data;
}

function writeData(path, data) {
    data = JSON.stringify(data, null, 2);
    fs.writeFile(path, data, (err) => {
        if (err) {
            console.log(err);
        }
    });
}


const matches = getData(path.resolve(__dirname, '../data/matches.csv'));
const deliveries = getData(path.resolve(__dirname, '../data/deliveries.csv'));


let result = ipl.getMatchesPerYear(matches);
writeData(path.resolve(__dirname,'../public/output/matchesPerYear.json'), result);

result = ipl.getMatchesWonPerTeamPerYear(matches);
writeData(path.resolve(__dirname,'../public/output/matchesWonPerTeamPerYear.json'), result);

result = ipl.getExtraRunsConcededPerTeam(matches, deliveries, 2016);
writeData(path.resolve(__dirname,'../public/output/extraRunsConcededPerTeam.json'), result);

result = ipl.getTopEconomicalBowlers(matches, deliveries, 2015, 10);
writeData(path.resolve(__dirname,'../public/output/topEconomicalBowlers.json'), result);

result = ipl.getTeamsWonTossAndMatch(matches);
writeData(path.resolve(__dirname,'../public/output/teamsWonTossAndMatch.json'), result);

result = ipl.getHighestPlayerOfMatchPerSeason(matches);
writeData(path.resolve(__dirname,'../public/output/highestPlayerOfMatchPerSeason.json'), result);

result = ipl.getBatsmenSRPerSeason(matches, deliveries, 'V Kohli');
writeData(path.resolve(__dirname,'../public/output/batsmenSRPerSeason.json'), result);

result = ipl.getPlayerDismissedByAnotherPlayer(deliveries);
writeData(path.resolve(__dirname,'../public/output/playerDismissedByAnotherPlayer.json'), result);

result = ipl.getBestEconomyInSuperOver(deliveries);
writeData(path.resolve(__dirname,'../public/output/bestEconomyInSuperOver.json'), result);