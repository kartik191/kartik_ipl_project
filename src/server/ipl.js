const fs = require('fs');


function matchesIdOfYear(matches, year){
    let matchesId = matches.reduce((result, match) => {
        let season = match.season;
        let id = match.id;  
        if(season == year){
            result[id] = true;
        }   
        return result;
    }, {}); 
    return matchesId;
}

function calculateEconomy(bowler){
    bowler[1] = bowler[1].totalRuns / (bowler[1].totalBalls / 6);
    return {'bowler': bowler[0], 'economy': bowler[1]};
}

function bowlerData(result, delivery){
    let bowler = delivery.bowler;
    let batsmenRuns = Number(delivery.batsman_runs);
    let wideRuns = Number(delivery.wide_runs);
    let noBallRuns = Number(delivery.noball_runs);
    
    if(result[bowler] === undefined){
        result[bowler] = {'totalBalls': 0, 'totalRuns': 0};
    }
    result[bowler].totalRuns += batsmenRuns + wideRuns + noBallRuns;
    if(wideRuns === 0 && noBallRuns === 0){
        result[bowler].totalBalls += 1;
    }
    return result;
}

function getSeasonOfAllMatchId(result, match){
    let matchId = match.id;
    let season = match.season;
    result[matchId] = season;
    return result;
}

function getRunsOfPlayer(matchIdYears){
    function innerGetRunsOfPlayer(result, delivery){
        let runs = Number(delivery.batsman_runs);
        let matchId = delivery.match_id;
        let season = matchIdYears[matchId];
        let wideRuns = delivery.wide_runs;
        let noBallRuns = delivery.noball_runs;
        if(result[season] === undefined){
            result[season] = {'totalBalls': 0, 'totalRuns': 0};
        }
        result[season].totalRuns += runs;
        if(wideRuns == 0 && noBallRuns == 0){
            result[season].totalBalls += 1;
        } 
        return result;
    }
    return innerGetRunsOfPlayer;
}

// Number of matches played per year for all the years in IPL.

function getMatchesPerYear(matches){
    let matchesPerYear = matches.reduce((result, match) => {
        let season = match.season;
        result[season] === undefined ? result[season] = 1 : result[season] += 1;
        return result;
    }, {});  
    return matchesPerYear;
}


// Number of matches won per team per year in IPL.

function getMatchesWonPerTeamPerYear(matches){
    let matchesWonPerTeamPerYear = matches.reduce((result, match) => {
        let winner = match.winner;
        let season = match.season;
        if(winner === ''){
            return result;
        }
        if(result[season] === undefined) {
            result[season] = {};
        }
        result[season][winner] === undefined ? result[season][winner] = 1 : result[season][winner] += 1;
        return result;
    }, {}); 
    return matchesWonPerTeamPerYear;
}


// Extra runs conceded per team in the year

function getExtraRunsConcededPerTeam(matches, deliveries, year){
    if(typeof(year) !== 'number'){
        year = 2016;
    }
    let matchesId = matchesIdOfYear(matches, year);
    let extraRunsConcededPerTeam = deliveries.reduce((result, delivery) => {
        let id = delivery.match_id;
        let bowlingTeam = delivery.bowling_team;
        let extraRuns = Number(delivery.extra_runs);
        if(matchesId[id] === true && extraRuns >= 1){
            result[bowlingTeam] === undefined ? result[bowlingTeam] = extraRuns : result[bowlingTeam] += extraRuns;
        }
        return result;
    }, {});
    return extraRunsConcededPerTeam;
}


// Top economical bowlers in the year

function getTopEconomicalBowlers(matches, deliveries, year, top){
    if(typeof(year) !== 'number'){
        year = 2015;
    }
    if(typeof(top) !== 'number'){
        top = 10;
    }
    let matchesId = matchesIdOfYear(matches, year); 
    let bowlersEconomy = deliveries.filter(delivery => {
        let id = delivery.match_id;
        return matchesId[id] === true;
    }).reduce(bowlerData, {}); 

    bowlersEconomy = Object.entries(bowlersEconomy)
        .map(calculateEconomy)
        .sort((a, b) => a.economy > b.economy ? 1 : -1)
        .slice(0,top);
    return bowlersEconomy;
}

//additional problems
//Find the number of times each team won the toss and also won the match

function getTeamsWonTossAndMatch(matches){
    let teamsWonTossAndMatch = matches.reduce((result, match) => {
        let tossWinner = match.toss_winner;
        let matchWinner = match.winner;
        if(tossWinner === matchWinner){
            result[matchWinner] === undefined ? result[matchWinner] = 1 : result[matchWinner] += 1;
        }
        return result;
    }, {});
    return teamsWonTossAndMatch;
}

// Find a player who has won the highest number of Player of the Match awards for each season

function getHighestPlayerOfMatchPerSeason(matches){
    let highestPlayerOfMatchPerSeason = matches.reduce((result, match) => {
        let season = match.season;
        let playerOfMatch = match.player_of_match;
        if(result[season] === undefined){
            result[season] = {};
        }
        result[season][playerOfMatch] === undefined ? result[season][playerOfMatch] = 1 : result[season][playerOfMatch] += 1;
        return result;
    }, {});
    highestPlayerOfMatchPerSeason = Object.entries(highestPlayerOfMatchPerSeason)
        .reduce((result, array) => {
            let season = array[0];
            let players = array[1];
            let playerOfMatch = Object.entries(players)
                .reduce((innerResult, playerArray) => {
                    let player = playerArray[0];
                    let count = playerArray[1];
                    if(count === innerResult.count){
                        innerResult.players.push(player);
                    }
                    else if(count > innerResult.count){
                        innerResult.players = [player];
                        innerResult.count = count;
                    }
                    return innerResult;
                },{'players': [], 'count': 0});
            result[season] = playerOfMatch;
            return result;
        }, {});
    return highestPlayerOfMatchPerSeason;
}

// Find the strike rate of a batsman for each season

function getBatsmenSRPerSeason(matches, deliveries, batsmen){
    let matchIdYears = matches.reduce(getSeasonOfAllMatchId, {}); 
    let batsmenSR = deliveries.filter(delivery => delivery.batsman === batsmen)
        .reduce(getRunsOfPlayer(matchIdYears), {});
    batsmenSR = Object.entries(batsmenSR)
        .reduce((result, array) => {
            let season = array[0];
            let SR = array[1];
            SR['strikeRate'] = (SR.totalRuns / SR.totalBalls) * 100;
            result[season] = SR;
            return result;
        }, {});
    return batsmenSR;
}

// Find the highest number of times one player has been dismissed by another player

function getPlayerDismissedByAnotherPlayer(deliveries){
    let bowlerDismissal = {'caught': 1, 'bowled': 1, 'lbw': 1, 'caught and bowled': 1, 'stumped': 1};

    let playerDismissedByAnotherPlayer = deliveries.reduce((result, delivery) => {
        let dismissalType = delivery.dismissal_kind;
        let batsmen = delivery.player_dismissed;
        let bowler = delivery.bowler;
        let fielder = delivery.fielder;
        let key;
        if(bowlerDismissal[dismissalType] === 1 || dismissalType === 'run out'){
            bowlerDismissal[dismissalType] === 1 ? key = batsmen +',' + bowler : key = batsmen +',' + fielder;
            result[key] === undefined ? result[key] = 1 : result[key] += 1;
        }
        return result;
    }, {});
    playerDismissedByAnotherPlayer = Object.entries(playerDismissedByAnotherPlayer)
        .reduce((result, pair) => {
            let count = pair[1];
            let batsmen = pair[0].split(',')[0];
            let dismissedBy = pair[0].split(',')[1]; 
            if(count === result.count){
                result.players.push({'batsmen': batsmen, 'dismissedBy': dismissedBy});
            }
            else if(count > result.count){
                result.players = [{'batsmen': batsmen, 'dismissedBy': dismissedBy}];
                result.count = count;
            }
            return result;
        }, {'players':[], 'count': 0}); 
    return playerDismissedByAnotherPlayer;
}

//Find the bowler with the best economy in super overs

function getBestEconomyInSuperOver(deliveries, top){
    if(typeof(top) !== 'number'){
        top = 1;
    }
    let bestEconomyInSuperOver = deliveries.filter(delivery => delivery.is_super_over == 1)
        .reduce(bowlerData, {});  
    
    bestEconomyInSuperOver = Object.entries(bestEconomyInSuperOver)
        .map(calculateEconomy)
        .sort((a, b) => a.economy > b.economy ? 1 : -1)
        .slice(0, top);
    return bestEconomyInSuperOver;
}


module.exports = { getTopEconomicalBowlers, getExtraRunsConcededPerTeam, getMatchesWonPerTeamPerYear, getMatchesPerYear,
    getTeamsWonTossAndMatch, getHighestPlayerOfMatchPerSeason, getBatsmenSRPerSeason, getPlayerDismissedByAnotherPlayer,
    getBestEconomyInSuperOver }; 